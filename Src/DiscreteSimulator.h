#pragma once
#include "Simulator.h"

class DiscreteSimulator : public Simulator
{
public:
	DiscreteSimulator(float timeStep);

	void ProgressTime(const double& timeStep) final;

protected:
	const double GetNextSimulationTime() const final;

private:
	const double _timeStep;
};

// Async Simulation test.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include "DiscreteSimulator.h"
#include "RootGovernor.h"

#include <iostream>

std::shared_ptr<DiscreteSimulator> CreateDiscreteSimulator(std::string tag, std::shared_ptr<SubGovernor> subGovernor) {
	auto discreteSimulator = subGovernor->AddSimulator<DiscreteSimulator>(tag, 0.1f);
	return discreteSimulator;
}

void CreateSubGovernor(RootGovernor* rootGovernor) {
	auto subGovernor = std::make_shared<SubGovernor>(0.001f, 120.0f);

	auto sim1 = CreateDiscreteSimulator("Sim1", subGovernor);
	auto sim2 = CreateDiscreteSimulator("Sim2", subGovernor);
	auto sim3 = CreateDiscreteSimulator("Sim3", subGovernor);
	auto sim4 = CreateDiscreteSimulator("Sim4", subGovernor);

	//subGovernor->AddSimulatorDependency(sim1, sim3);
	//subGovernor->AddSimulatorDependency(sim3, sim2);
	subGovernor->AddSimulatorDependency(sim4, sim1);

	rootGovernor->AddSubGovernor(subGovernor);
}

int main()
{
	RootGovernor* governor = new RootGovernor();;

	CreateSubGovernor(governor);

	for (int i = 0; i < 1000; i++)
	{
		CreateSubGovernor(governor);
	}

	//while (governor->CurrentlyActiveSubGovernors() > 400) {
	//	governor->Update();
	//	//std::cin.get();
	//}

	//for (int i = 0; i < 1500; i++)
	//{
	//	CreateSubGovernor(governor);
	//}

	while (governor->CurrentlyActiveSubGovernors() > 0) {
		governor->Update();
		//std::cin.get();
	}

	delete governor;
}

// Run program: Ctrl + F5 or Debug > Start Without Debugging menu
// Debug program: F5 or Debug > Start Debugging menu

// Tips for Getting Started:
//   1. Use the Solution Explorer window to add/manage files
//   2. Use the Team Explorer window to connect to source control
//   3. Use the Output window to see build output and other messages
//   4. Use the Error List window to view errors
//   5. Go to Project > Add New Item to create new code files, or Project > Add Existing Item to add existing code files to the project
//   6. In the future, to open this project again, go to File > Open > Project and select the .sln file
#pragma once

#include "Simulator.h"
#include "entt/entt.hpp"

class Simulator;

class Scene
{
public:
	Scene(Simulator* simulator);
	void Update(const double& timeStep);

private:
	entt::registry _sceneRegistery;
	Simulator* _simulator;
};

#include "Simulator.h"

#include "SubGovernor.h"
#include "Scene.h"

#include <memory>
#include <iostream>
#include <sstream>

Simulator::SynchronisationInfo::SynchronisationInfo()
	: RemainingSynchronisationJobs(0)
{
}

void Simulator::SynchronisationInfo::EnqueueJob(Job* job)
{
	_jobMutex.lock();
	RemainingSynchronisationJobs++;
	SynchronisationJobs.push_back(job);
	_jobMutex.unlock();
}

void Simulator::SynchronisationInfo::Synchronize()
{
	while (RemainingSynchronisationJobs > 0) {
		bool locked = _jobMutex.try_lock();
		while (!locked) {
			// retry to accquire lock, in meantime execute jobs from jobsystem
			GetJobSystem()->ExecuteJob(JobPriority::Normal);
			locked = _jobMutex.try_lock();
		}

		for (int i = 0; i < (int)SynchronisationJobs.size(); i++)
		{
			Job*& job = SynchronisationJobs[i];
			job->Run();
			job->Free();
			RemainingSynchronisationJobs--;
		}
		SynchronisationJobs.clear();
		_jobMutex.unlock();
	}
}

Simulator* Simulator::FindDependency(std::string tag)
{
	Simulator* possibleDependency = _parentGovernor->FindSimulator(tag);
	if (possibleDependency == nullptr)
		return nullptr;

	if (_dependencies.contains(possibleDependency->_id))
		return possibleDependency;

	return nullptr;
}

void Simulator::AddSynchronisationStep(void(*syncFunction)(const Scene*))
{
	const Scene* scene = simulatorScene;
	Job* syncJob = JobSystem::CreateJob(JobPriority::High, syncFunction, scene);
	_synchronisationInfo.EnqueueJob(syncJob);
}

std::atomic<int> SimulatorIDFactory = 0;
Simulator::Simulator()
	: currentTime(0), _id(SimulatorIDFactory++), _parentGovernor(nullptr), latestStepTime(0), _synchronisationInfo()
{
	// schedule empty jobs to set a starting point to not have a deadlock
	_latestUpdateJob.store(GetJobSystem()->Schedule(JobSystem::CreateJob([]() {})));
	_latestSyncJob.store(GetJobSystem()->Schedule(JobSystem::CreateJob([]() {})));
	simulatorScene = new Scene(this);
}

Simulator::~Simulator()
{
	// wait for jobs to finish
	GetJobSystem()->WaitForJobCompletion(_latestSyncJob.load());
	GetJobSystem()->WaitForJobCompletion(_latestUpdateJob.load());

	delete simulatorScene;
}

void Simulator::Update(const double& timeStep)
{
	const double nextSimulationTime = GetNextSimulationTime();

	currentTime += timeStep;
	if (currentTime < nextSimulationTime)
		return; // no update is needed

	double* progressedTime = new double(currentTime - latestStepTime);
	auto updateJob = [](Simulator* simulator, Scene* simulatorScene, double* timeStep) {
		simulatorScene->Update(*timeStep);
		simulator->ProgressTime(*timeStep);

		delete timeStep;
	};

	latestStepTime = currentTime;
	_latestUpdateJob.store(
		GetJobSystem()->Schedule(
			JobSystem::CreateJob(
				JobPriority::High,
				updateJob,
				this, simulatorScene, progressedTime),
			_latestSyncJob.load()
		)
	);
}

void Simulator::Synchronize()
{
	// Do not update while simulator is busy
	if (_synchronisationInfo.RemainingSynchronisationJobs == 0)
		return;

	_latestSyncJob.store(
		GetJobSystem()->Schedule(
			JobSystem::CreateJob(
				JobPriority::High,
				[](Simulator::SynchronisationInfo* syncInfo) {
					syncInfo->Synchronize();
				}, &_synchronisationInfo),
			_latestUpdateJob.load()
					)
	);
}

void Simulator::WaitForEndOfUpdate()
{
	GetJobSystem()->WaitForJobCompletion(_latestUpdateJob.load());
}

void Simulator::WaitForEndOfSynchronisation()
{
	GetJobSystem()->WaitForJobCompletion(_latestSyncJob.load());
}

const double& Simulator::GetCurrentTime()
{
	return currentTime;
}

bool Simulator::IsSynchronizing()
{
	return !GetJobSystem()->IsJobCompleted(_latestSyncJob.load());
}

bool Simulator::IsUpdating()
{
	return !GetJobSystem()->IsJobCompleted(_latestUpdateJob.load());
}

void Simulator::AttachSubGovernor(SubGovernor* subgovernor)
{
	_parentGovernor = subgovernor;
}

void Simulator::AddDependency(const Simulator* dependency)
{
	int id = dependency->_id;
	if (!_dependencies.contains(id))
		_dependencies.emplace(id);
}
#include "DiscreteSimulator.h"

#include <iostream>
#include <sstream>

DiscreteSimulator::DiscreteSimulator(float timeStep)
	: _timeStep(timeStep)
{
}

void DiscreteSimulator::ProgressTime(const double& timeStep)
{
	/*for (currentTime = fromSimulationTime; currentTime < toSimulationTime; currentTime += _timeStep)
	{*/

	//std::stringstream outputStream;
	//outputStream << "updating time: " << currentTime;
	//outputStream << std::endl;
	//printf(outputStream.str().c_str());

	auto dependency = FindDependency("sim1");
	if (dependency != nullptr) {
		dependency->AddSynchronisationStep([](const Scene* scene) -> void {
			//std::stringstream outputStream;
			//outputStream << "Performing sync step";
			//outputStream << std::endl;
			//printf(outputStream.str().c_str());
			});
	}
	//}
}

const double DiscreteSimulator::GetNextSimulationTime() const
{
	return latestStepTime + _timeStep;
}
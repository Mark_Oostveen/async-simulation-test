#include "SubGovernor.h"

#include "RootGovernor.h"

#include <sstream>
#include <iostream>

SubGovernor::SubGovernor(const double smallestTimeStep, const double endTime)
	: SmallestTimeStep(smallestTimeStep), EndTime(endTime)
{
	_latestUpdateJob.store(GetJobSystem()->Schedule(JobSystem::CreateJob([]() {})));
}

void SubGovernor::Update()
{
	if (!GetJobSystem()->IsJobCompleted(_latestUpdateJob.load()))
		return;

	Job* updateJob = JobSystem::CreateJob(
		JobPriority::Low,
		[](std::vector<std::shared_ptr<Simulator>>* simulators, const double* smallestTimeStep, std::atomic<double>* currentTime) {
			// Progress time
			currentTime->store(*smallestTimeStep + currentTime->load());

			for (int i = 0; i < simulators->size(); i++)
			{
				simulators->at(i)->WaitForEndOfSynchronisation();
			}

			for (int i = 0; i < simulators->size(); i++)
			{
				simulators->at(i)->Update(*smallestTimeStep);
			}

			for (int i = 0; i < simulators->size(); i++)
			{
				simulators->at(i)->WaitForEndOfUpdate();
			}

			for (int i = 0; i < simulators->size(); i++)
			{
				simulators->at(i)->Synchronize();
			}
		}, &_simulators, &SmallestTimeStep, &_currentTime);

	_latestUpdateJob.store(GetJobSystem()->Schedule(updateJob));
}

void SubGovernor::AddSimulatorDependency(std::shared_ptr<Simulator> simulator, std::shared_ptr<Simulator> dependency)
{
	simulator->AddDependency(dependency.get());
}

Simulator* SubGovernor::FindSimulator(int id)
{
	return _simulators[_simulatoridIndexLookup[id]].get();
}

Simulator* SubGovernor::FindSimulator(std::string tag)
{
	std::transform(tag.begin(), tag.end(), tag.begin(), [](unsigned char c) {return std::tolower(c); });
	if (_simulatorTagIdLookup.contains(tag))
		return FindSimulator(_simulatorTagIdLookup.at(tag));
	else
		return nullptr;
}

bool SubGovernor::IsDone()
{
	return _currentTime.load() >= EndTime;
}

double SubGovernor::GetCurrentTime() const
{
	return _currentTime.load();
}
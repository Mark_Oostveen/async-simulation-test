#pragma once

#include "JobSystem/JobSystem.h"

#define WorkerThreads 2

JbSystem::JobSystem* GetJobSystem();
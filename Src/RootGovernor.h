#pragma once

#include "SubGovernor.h"

#include <vector>
#include <memory>

class RootGovernor
{
public:

	void Update();

	void AddSubGovernor(std::shared_ptr<SubGovernor> governor);
	void RemoveSubGovernor(std::shared_ptr<SubGovernor> governor);

	int CurrentlyActiveSubGovernors();

private:
	std::vector<std::shared_ptr<SubGovernor>> _subGovernors;
};

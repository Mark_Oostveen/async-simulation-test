#include "AsyncSimulator.h"

static JbSystem::JobSystem* simulatorJobSystemInstance = new JbSystem::JobSystem(WorkerThreads);

JbSystem::JobSystem* GetJobSystem()
{
	return simulatorJobSystemInstance;
}
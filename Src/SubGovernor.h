#pragma once

#include "Simulator.h"

#include <vector>
#include <memory>
#include <type_traits>
#include <unordered_map>
#include <string>

class SubGovernor
{
	friend RootGovernor;

public:
	SubGovernor(const double smallestTimeStep, const double endTime);

	void Update();

	template<SimulatorConcept T, class... Args>
	inline std::shared_ptr<T> AddSimulator(std::string tag, Args... args) {
		std::shared_ptr<T> simulator = std::make_shared<T>(std::forward<Args>(args)...);
		simulator->AttachSubGovernor(this);

		std::transform(tag.begin(), tag.end(), tag.begin(), [](unsigned char c) {return std::tolower(c); });

		// Register the simulator in lookup tables
		if (_simulatorTagIdLookup.contains(tag))
			throw std::exception("Tag is already used, unique tag for this subgovernor is required");

		_simulatorTagIdLookup.emplace(tag, simulator->_id);
		_simulatoridIndexLookup.emplace(simulator->_id, (int)_simulators.size());
		_simulators.push_back(simulator);

		return simulator;
	}

	void AddSimulatorDependency(std::shared_ptr<Simulator> simulator, std::shared_ptr<Simulator> dependency);

	Simulator* FindSimulator(int id);
	Simulator* FindSimulator(std::string tag);

	bool IsDone();
	double GetCurrentTime() const;
	const double EndTime;
	const double SmallestTimeStep;

private:

	// Simulator lookups
	std::unordered_map<std::string, int> _simulatorTagIdLookup;
	std::unordered_map<int, int> _simulatoridIndexLookup;
	std::vector<std::shared_ptr<Simulator>> _simulators;

	std::atomic<double> _currentTime;

	std::atomic<int> _latestUpdateJob;
};

#pragma once

#include "AsyncSimulator.h"

#include <atomic>
#include <mutex>
#include <unordered_set>

using namespace JbSystem;

class SubGovernor;
class RootGovernor;
class Scene;

class Simulator
{
	friend class SubGovernor;
	friend class RootGovernor;

public:
	struct SynchronisationInfo {
		SynchronisationInfo();

		void EnqueueJob(JbSystem::Job* job);

		void Synchronize();

		std::atomic<int> RemainingSynchronisationJobs;
		std::vector<JbSystem::Job*> SynchronisationJobs;

	private:
		std::mutex _jobMutex;
	};

public:
	virtual ~Simulator();

	/// <summary>
	/// Update the simulator
	/// NOTE* Please make sure simulator is not updating already
	/// </summary>
	/// <param name="timeStep"></param>
	void Update(const double& timeStep);

	/// <summary>
	/// Synchronise the simulator with other simulators
	/// NOTE* Please make sure simulator is not synchonizing already
	/// </summary>
	void Synchronize();
	void WaitForEndOfUpdate();
	void WaitForEndOfSynchronisation();

	const double& GetCurrentTime();

	virtual void ProgressTime(const double& timeStep) = 0;

	Simulator* FindDependency(std::string tag);

	template<class... Args>
	inline void AddSynchronisationStep(void(*syncFunction)(const Scene*, Args...), Args... args) {
		const Scene* scene = simulatorScene;
		Job* syncJob = JobSystem::CreateJob(JobPriority::High, syncFunction, scene, std::forward<Args>(args)...);
		_synchronisationInfo.EnqueueJob(syncJob);
	}

	void AddSynchronisationStep(void(*syncFunction)(const Scene*));

	bool IsSynchronizing();
	bool IsUpdating();

	void AttachSubGovernor(SubGovernor* subgovernor);

protected:
	Simulator();

	virtual const double GetNextSimulationTime() const = 0;
	double currentTime;
	double latestStepTime;
	Scene* simulatorScene;

private:
	void AddDependency(const Simulator* dependency);

	SubGovernor* _parentGovernor;

	const int _id;

	SynchronisationInfo _synchronisationInfo;

	std::atomic<int> _latestSyncJob;
	std::atomic<int> _latestUpdateJob;

	std::unordered_set<int> _dependencies;
};

template<class T>
concept SimulatorConcept = std::is_base_of<Simulator, T>::value;
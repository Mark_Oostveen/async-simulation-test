#include "RootGovernor.h"

#include <sstream>
#include <iostream>

void RootGovernor::Update()
{
	for (int i = 0; i < (int)_subGovernors.size(); i++)
	{
		_subGovernors[i]->Update();
	}

	for (int i = 0; i < (int)_subGovernors.size(); i++)
	{
		auto subGovernor = _subGovernors[i];
		if (subGovernor->IsDone())
			RemoveSubGovernor(subGovernor);
	}
}

void RootGovernor::AddSubGovernor(std::shared_ptr<SubGovernor> governor)
{
	_subGovernors.push_back(governor);
}

void RootGovernor::RemoveSubGovernor(std::shared_ptr<SubGovernor> governor)
{
	for (int i = 0; i < _subGovernors.size(); i++)
	{
		if (_subGovernors[i] == governor) {
			_subGovernors.erase(_subGovernors.begin() + i);

			// Make sure that the entire update cycle is complete

			GetJobSystem()->WaitForJobCompletion(governor->_latestUpdateJob);
			for (int i = 0; i < governor->_simulators.size(); i++)
			{
				GetJobSystem()->WaitForJobCompletion(governor->_simulators[i]->_latestUpdateJob.load());
				GetJobSystem()->WaitForJobCompletion(governor->_simulators[i]->_latestSyncJob.load());
			}

			std::stringstream outputStream;
			outputStream << governor.get() << " SubGovernor is finished";
			outputStream << std::endl;
			printf(outputStream.str().c_str());
		}
	}
}

int RootGovernor::CurrentlyActiveSubGovernors()
{
	return (int)_subGovernors.size();
}